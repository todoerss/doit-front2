import {Profile} from "../auth/Models";

export class TodoGroup {
    id: number
    name: string
    created_at: Date
    updated_at: Date
    created_by: Profile | null
    updated_by: Profile | null


    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;
        this.created_by = data.created_by ? new Profile(data.created_by) : null;
        this.updated_by = data.updated_by ? new Profile(data.updated_by) : null;
    }

    serialize(): object {
        return {
            name: this.name,
        }
    }
}

export class TodoList {
    id: number
    group: number
    name: string
    created_at: Date
    updated_at: Date
    created_by: Profile | null
    updated_by: Profile | null


    constructor(data: any) {
        this.id = data.id;
        this.group = data.group;
        this.name = data.name;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;
        this.created_by = data.created_by ? new Profile(data.created_by) : null;
        this.updated_by = data.updated_by ? new Profile(data.updated_by) : null;
    }

    serialize(): object {
        return {
            group: this.group,
            name: this.name,
        }
    }
}

export class TodoEntry {
    id: number
    list: number
    name: string
    body: string
    is_done: boolean
    created_at: Date
    updated_at: Date
    created_by: Profile | null
    updated_by: Profile | null


    constructor(data: any) {
        this.id = data.id;
        this.list = data.list;
        this.name = data.name;
        this.body = data.body;
        this.is_done = data.is_done;
        this.created_at = data.created_at;
        this.updated_at = data.updated_at;
        this.created_by = data.created_by ? new Profile(data.created_by) : null;
        this.updated_by = data.updated_by ? new Profile(data.updated_by) : null;
    }

    serialize(): object {
        return {
            list: this.list,
            name: this.name,
            body: this.body,
            is_done: this.is_done,
        }
    }
}
