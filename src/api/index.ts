import axios from 'axios'

export const apiURL = import.meta.env.VITE_API_URL || 'http://localhost:8000/'
export const websocketURL =
    (apiURL.startsWith('https://') ? 'wss://' : 'ws://')
    + apiURL.replace('http://', '').replace('https://', '')

export const apiClient = axios.create({
    baseURL: apiURL,
    headers: {
        'Content-Type': 'application/json'
    },
    xsrfCookieName: 'csrftoken',
    xsrfHeaderName: 'X-CSRFToken'
})

export class ErrorResponse {
    errors: Map<string, any>

    constructor(errors: object) {
        this.errors = new Map<string, any>()

        console.log('original errors', errors)

        for (let key in errors) {
            if (errors.hasOwnProperty(key)) {
                // @ts-ignore
                this.errors.set(key, errors[key])
            }
        }
    }
}

const getToken = () => {
    const token = document.cookie.match(/doit-token=([^ ;]*)/)
    if (token && token.length > 0) {
        return token[1]
    }
    return null
}

const setToken = (token: string) => {
    document.cookie = `doit-token=${token}; path=/`
}

export const deleteToken = () => {
    document.cookie.split(';').forEach(function (c) {
        document.cookie = c
            .replace(/^ +/, '')
            .replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/')
    })
}

apiClient.interceptors.request.use((request) => {
    const token = getToken()
    if (token && request.url !== '/login/') {
        request.headers!!.Authorization = `Token ${token}`
    }
    return request
})

apiClient.interceptors.response.use(
    ({config, data}) => {
        if (config.url === '/login/')
            setToken(data.token)
        return data
    },
    (error) => {
        if (error) {
            // Redirect to login form when no credentials are available
            if (error.response.data.detail && (
                error.response.data.detail === "Authentication credentials were not provided." ||
                error.response.data.detail === "Invalid token."
            )) {
                // if is okay to request current_profile as a check if user is signed in
                if (error.config.url !== '/current_profile/')
                    window.location.href = `/login/`
            }

            const errorData = error.response.data || {}
            return Promise.reject(new ErrorResponse(errorData))
        }
        return error
    }
)


export class ServerUpdate {
    datetime: Date
    entry: any
    event_type: string
    entity_name: string
    subsystem: string

    constructor(data: any) {
        this.datetime = data.datetime;
        this.entry = data.entry;
        this.event_type = data.event_type;
        this.entity_name = data.entity_name;
        this.subsystem = data.subsystem;
    }
}