export class Profile {
    id: number
    first_name: string
    last_name: string
    email: string


    constructor(data: any) {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.email = data.email;
    }

    serialize() {
        return {
            first_name: this.first_name,
            last_name: this.last_name,
            email: this.email,
        }
    }
}

export class LoginResponse {
    token: string
    profile: Profile

    constructor(data: any) {
        this.token = data.token
        this.profile = new Profile(data.profile)
    }
}