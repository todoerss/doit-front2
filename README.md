# doit frontend

Table of contents

1. [About the Project](#about)
    1. [Built and Tested With](#built-with)
2. [Getting Started](#getting-started)
    1. [Installation](#installation)
3. [Usage](#usage)
4. [Quality Assurance](#quality)
5. [CI/CD](#ci)
7. [Contributing](#contributing)
8. [License](#license)
9. [Contact](#contact)
10. [Acknowledgements](#acknowledgements)

## About the Project <a name="about"></a>

doit is a universal task manager for all your needs: 
- organise your workflow;
- free your mind from duties;
- bring a balance to your life. 

### Built and Tested with <a name="built-with"></a>

- TypeScript (Vue 3 framework)
- Cypress (frontend)
- Vite (building)

## Getting Started <a name="getting-started"></a>

In order to run the app locally, follow the next steps.

### Preparing development environment <a name="installation"></a>

1. Clone repository
```bash
git clone https://gitlab.com/todoerss/doit-front2
```

2. Then, you need to install NodeJS and npm:

```shell
npm install -g n
n lts
```

## Usage <a name="usage"></a>

To run the frontend, you need to launch the backend part, first. See instructions on https://gitlab.com/todoerss/doit. After that execute:

```shell
npm run dev
```

Visit http://localhost:3000. Voi la!

## Quality Assurance <a name="quality"></a>

To ensure quality of code written we utilized tools for linting (```eslint```) and static code analysis (```SonarCloud```).

To test the frontend, we used Cypress and wrote integration tests with stubs.

## CI/CD <a name="ci"></a>
We launched two types of servers. The first one, called "stage", is used to imitate production environment and test the functionality. The second one is used for production.

In CI/CD pipelines, there are stages for linting, static code analysis, testing, building, pushing to DockerHub and deployment for each type of servers.

## Contributing <a name="contributing"></a>

Contributions are welcome! Teamwork and collaboration boost development of ideas and accelerate improvement of projects. If you would like to contribute to the project, follow the steps:

1. Fork the Project
2. Create a separate branch for your feature (```git checkout -b feature/YourFeature```)
3. Commit your Changes (```git commit -m 'Added some feature'```)
4. Push to the Branch (```git push origin feature/YourFeature```)
5. Open a Pull Request

For major modifications, open an issue to discuss what you want to change.

## Contacts <a name="contact"></a>

- Grigoriy Dolgov g.dolgov@innopolis.university
- Anna Gorb a.gorb@innopolis.university
- Marina Smirnova m.smirnova@innopolis.university
- Maxim Stepanov m.stepanov@innopolis.university