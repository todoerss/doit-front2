import {ServerUpdate, websocketURL} from "../index";
import {Subject} from "rxjs";
import {TodoEntry, TodoGroup, TodoList} from "./Models";

export class TodoSubjects {
    root_subject = new Subject<any>()

    entryUpdated = new Subject<TodoEntry>()
    entryCreated = new Subject<TodoEntry>()
    entryDeleted = new Subject<TodoEntry>()
    entryReordered = new Subject<TodoEntry>()

    listUpdated = new Subject<TodoList>()
    listCreated = new Subject<TodoList>()
    listDeleted = new Subject<TodoList>()
    listReordered = new Subject<TodoList>()

    groupUpdated = new Subject<TodoGroup>()
    groupCreated = new Subject<TodoGroup>()
    groupDeleted = new Subject<TodoGroup>()
    groupReordered = new Subject<TodoGroup>()
}

export function createTodoSubjects(): TodoSubjects {
    let websocket = new WebSocket(websocketURL + "ws/todo_updates/");

    let subjects = new TodoSubjects()

    websocket.onopen = function (evt) {
        console.log('websocket opened', evt)
    };
    websocket.onclose = function (evt) {
        console.log('websocket closed', evt)
    };
    websocket.onmessage = function (evt) {
        let data = JSON.parse(evt.data)
        console.log('onMessage JSON', data)

        let obj = dispatchTodoMessage(data)
        if (obj === false) {
            console.warn(`Unknown message type received for subsystem ${data.subsystem}: ${data.event_type}`)
        } else {
            // TODO: fix this. Boolean gets into root subject
            subjects.root_subject.next(obj)
        }
    };
    websocket.onerror = function (evt) {
        console.log('onError', evt)
    };

    function dispatchTodoMessage(update: ServerUpdate): boolean {
        switch (update.entity_name) {
            case 'todo_entry':
                switch (update.event_type) {
                    case 'entity.create':
                        subjects.entryCreated.next(new TodoEntry(update.entry))
                        return true
                    case 'entity.update':
                        subjects.entryUpdated.next(new TodoEntry(update.entry))
                        return true
                    case 'entity.delete':
                        subjects.entryDeleted.next(new TodoEntry(update.entry))
                        return true
                    case 'entity.reorder':
                        subjects.entryReordered.next(new TodoEntry(update.entry))
                        return true
                }
                return true
            case 'todo_list':
                switch (update.event_type) {
                    case 'entity.create':
                        subjects.listCreated.next(new TodoList(update.entry))
                        return true
                    case 'entity.update':
                        subjects.listUpdated.next(new TodoList(update.entry))
                        return true
                    case 'entity.delete':
                        subjects.listDeleted.next(new TodoList(update.entry))
                        return true
                    case 'entity.reorder':
                        subjects.listReordered.next(new TodoList(update.entry))
                        return true
                }
                return true
            case 'todo_group':
                switch (update.event_type) {
                    case 'entity.create':
                        subjects.groupCreated.next(new TodoGroup(update.entry))
                        return true
                    case 'entity.update':
                        subjects.groupUpdated.next(new TodoGroup(update.entry))
                        return true
                    case 'entity.delete':
                        subjects.groupDeleted.next(new TodoGroup(update.entry))
                        return true
                    case 'entity.reorder':
                        subjects.groupReordered.next(new TodoGroup(update.entry))
                        return true
                }
                return true
        }
        return false
    }

    return subjects
}
