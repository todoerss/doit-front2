import {createRouter, createWebHistory} from 'vue-router'

const routes = [
    {
        path: '/',
        name: 'about',
        component: () => import('~/components/common/About.vue'),
    },
    {
        path: '/login/',
        name: 'login',
        component: () => import('~/components/common/Login.vue'),
    },
    {
        path: '/todo/:group_id?/',
        name: 'todo',
        component: () => import('~/components/todo/TodoView.vue'),
    },
    {
        path: '/boards/',
        name: 'boards',
        component: () => import('~/components/board/BoardView.vue'),
    },
    {
        path: '/settings/',
        name: 'settings',
        component: () => import('~/components/common/Settings.vue'),
    }
]

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL || '/'),
    routes
})

export default router