import {TodoGroup} from "../todo/Models";
import {apiClient, deleteToken, ErrorResponse} from "../index";
import {LoginResponse, Profile} from "./Models";


class AuthApi {
    currentProfile = async (): Promise<Profile | null> =>
        apiClient
            .get(`/current_profile/`)
            .then(
                response => new Profile(response),
                reason => null,
            );

    updateCurrentProfile = async (profile: Profile): Promise<Profile> =>
        apiClient
            .put('/current_profile/', profile.serialize())
            .then(response => new Profile(response))

    login = async (login: string, password: string): Promise<LoginResponse | ErrorResponse> =>
        apiClient
            .post('/login/', {
                username: login,
                password: password
            })
            .then(
                // on fulfilled
                response => new LoginResponse(response),
                // on rejected
                response => response,
            )

    logout = () => {
        deleteToken()
    }
}

export const authApi = new AuthApi()
