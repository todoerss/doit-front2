import {apiClient} from "../index";
import {TodoEntry, TodoGroup, TodoList} from "./Models";
import {createTodoSubjects} from "./Messages";

class TodoApi {
    listGroups = async (): Promise<Array<TodoGroup>> =>
        apiClient
            .get(`/todo/groups/`)
            .then(response => response.map((d: any) => new TodoGroup(d)))

    getGroup = async (id: number): Promise<TodoGroup> =>
        apiClient
            .get(`/todo/groups/${id}/`)
            .then(response => new TodoGroup(response))

    createGroup = async (group: TodoGroup): Promise<TodoGroup> =>
        apiClient
            .post(`/todo/groups/`, {...group.serialize()})
            .then(response => new TodoGroup(response))

    patchGroup = async (group: TodoGroup): Promise<TodoGroup> =>
        apiClient
            .patch(`/todo/groups/${group.id}/`, {...group.serialize()})
            .then(response => new TodoGroup(response))

    deleteGroup = async (group: TodoGroup): Promise<void> =>
        apiClient
            .delete(`/todo/groups/${group.id}/`)


    listLists = async (): Promise<Array<TodoList>> =>
        apiClient
            .get(`/todo/lists/`)
            .then(response => response.map((d: any) => new TodoList(d)))

    getList = async (id: number): Promise<TodoGroup> =>
        apiClient
            .get(`/todo/lists/${id}/`)
            .then(response => new TodoList(response))

    listListsForGroup = async (group_id: number): Promise<Array<TodoList>> =>
        apiClient
            .get(`/todo/lists/for_group/${group_id}/`)
            .then(response => response.map((d: any) => new TodoList(d)))

    createList = async (list: TodoList): Promise<TodoList> =>
        apiClient
            .post(`/todo/lists/`, {...list.serialize()})
            .then(response => new TodoList(response))

    patchList = async (list: TodoList): Promise<TodoGroup> =>
        apiClient
            .patch(`/todo/lists/${list.id}/`, {...list.serialize()})
            .then(response => new TodoGroup(response))

    deleteList = async (list: TodoList): Promise<void> =>
        apiClient
            .delete(`/todo/lists/${list.id}/`)


    listEntriesForList = async (list_id: number): Promise<Array<TodoEntry>> =>
        apiClient
            .get(`/todo/entries/for_list/${list_id}/`)
            .then(response => response.map((d: any) => new TodoEntry(d)))

    createEntry = async (entry: TodoEntry): Promise<TodoEntry> =>
        apiClient
            .post(`/todo/entries/`, {...entry.serialize()})
            .then(response => new TodoEntry(response))

    patchEntry = async (entry: TodoEntry): Promise<TodoEntry> =>
        apiClient
            .patch(`/todo/entries/${entry.id}/`, {...entry.serialize()})
            .then(response => new TodoEntry(response))

    deleteEntry = async (entry: TodoEntry): Promise<void> =>
        apiClient
            .delete(`/todo/entries/${entry.id}/`)
}

export const todoApi = new TodoApi()
export const todoSubjects = createTodoSubjects()
